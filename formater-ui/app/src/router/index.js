import Vue from 'vue'
import Router from 'vue-router'
import Start from '@/components/Start'
import Running from '@/components/Running'
import Done from '@/components/Done'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'Start',
      component: Start
    }, {
      path: '/running',
      name: 'Running',
      component: Running
    }, {
      path: '/done',
      name: 'Done',
      component: Done
    }
  ]
})
