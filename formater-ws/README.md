# WebService Container

To build : `docker build . -t ws-container`

To run container : `docker run -p 5022:5022 ws-container`

Access with `http://localhost:5022`
