package org.ipgp.formater.pojo.ws;

/**
 * Created by olivier on 05/05/2017.
 */
public class PepsDataId {

    private String id;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
