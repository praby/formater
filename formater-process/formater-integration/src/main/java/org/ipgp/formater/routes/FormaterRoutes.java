package org.ipgp.formater.routes;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.component.jackson.JacksonDataFormat;
import org.apache.camel.component.restlet.RestletBinding;
import org.apache.camel.component.restlet.RestletConstants;
import org.apache.camel.model.dataformat.JsonLibrary;
import org.camunda.bpm.engine.HistoryService;
import org.camunda.bpm.engine.RepositoryService;
import org.camunda.bpm.engine.RuntimeService;
import org.camunda.bpm.engine.history.HistoricActivityInstance;
import org.camunda.bpm.engine.history.HistoricActivityInstanceQuery;
import org.camunda.bpm.engine.repository.ProcessDefinition;
import org.camunda.bpm.engine.rest.dto.AtomLink;
import org.camunda.bpm.engine.rest.dto.LinkableDto;
import org.camunda.bpm.engine.rest.dto.history.HistoricActivityInstanceDto;
import org.camunda.bpm.engine.rest.dto.runtime.ProcessInstanceDto;
import org.camunda.bpm.engine.rest.dto.runtime.VariableInstanceDto;
import org.camunda.bpm.engine.runtime.ProcessInstance;
import org.camunda.bpm.engine.runtime.VariableInstance;
import org.ipgp.formater.pojo.FormaterProcessDefinition;
import org.ipgp.formater.pojo.StartInterferogramme;
import org.ipgp.formater.pojo.ws.PepsDataId;
import org.ipgp.formater.pojo.ws.WS0In;
import org.ipgp.formater.pojo.ws.WS0Out;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by olivier on 29/03/2017.
 */
@Service
public class FormaterRoutes extends RouteBuilder {

    @Value("${formater.host}")
    private String formaterHost;

    @Value("${formater.ws0.port}")
    private String formaterWS0Port;

    @Value("${formater.ws1.port}")
    private String formaterWS1Port;


    @Value("${formater.root}")
    private String formaterPrefix;

    @Value("${formater.ws0}")
    private String formaterWs0;

    @Value("${formater.ws1}")
    private String formaterWs1;

    @Value("${formater.username}")
    private String userName;

    @Value("${formater.password}")
    private String password;

    @Autowired
    private RepositoryService repositoryService;

    @Autowired
    private RuntimeService runtimeService;


    @Autowired
    private HistoryService historyService;

    public void configure() throws Exception {

        restConfiguration().component("restlet");

        rest("/process")
                .post()
                .to("log:$body");

        rest("/process")
                .get()
                .to("direct:getProcessList");

        rest("/process/{id}/bpmn")
                .get()
                .to("direct:getProcessBpmn");

        rest("/process/{id}/processInstance").post()
                .to("log:$body")
                .to("direct:mapStartProcess");

        rest("/processInstance")
                .get()
                .enableCORS(true)
                .to("log:$body")
                .to("direct:getProcessInstances");

        rest("/processInstance/{instanceId}")
                .get()
                .to("log:$body")
                .to("direct:getProcessInstance");

        rest("/processInstance/{instanceId}/variable/")
                .get()
                .to("log:$body")
                .to("direct:getVariableInstances");

        rest("/processInstance/{instanceId}/activity/")
                .get()
                .to("log:$body")
                .enableCORS(true)
                .to("direct:getActivities");

        from("direct:mapStartProcess")
                .unmarshal().json(JsonLibrary.Jackson, StartInterferogramme.class)
                .removeHeaders("Camel.*")
                .to("direct:startProcess");

        from("direct:startProcess")
                .process(new Processor() {
                    @Override
                    public void process(Exchange exchange) throws Exception {
                        exchange.getOut().setHeaders(exchange.getIn().getHeaders());

                        Map<String, Object> requestParams = new HashMap<>();

                        StartInterferogramme startInterferogramme = exchange.getIn().getBody(StartInterferogramme.class);

                        requestParams.put("email", startInterferogramme.getEmail());
                        requestParams.put("images", startInterferogramme.getImages());

                        exchange.getOut().setBody(requestParams);
                    }
                })
                .to("log:$body?level=INFO&showAll=true")
                .toD(simple("camunda-bpm://start?processDefinitionKey=${in.headers.id}&copyBodyAsVariable=request").getText());

        from("direct:getProcessBpmn")
                .process(new Processor() {
                    @Override
                    public void process(Exchange exchange) throws Exception {
                        ProcessDefinition processDefinition = repositoryService.createProcessDefinitionQuery().processDefinitionKey((String) exchange.getIn().getHeader("id")).latestVersion().singleResult();
                        InputStream is = repositoryService.getProcessModel(processDefinition.getId());
                        exchange.getOut().setBody(is);
                    }
                });

        from("direct:getProcessList").process(new Processor() {

            public void process(Exchange exchange) throws Exception {
                List<ProcessDefinition> processDefinitions = repositoryService.createProcessDefinitionQuery().list();

                if (processDefinitions == null) {
                    exchange.getOut().setHeader("status", "204");
                }


                List<FormaterProcessDefinition> processDefinitionList = new ArrayList<>();
                for (ProcessDefinition processDefinition : processDefinitions) {
                    processDefinitionList.add(new FormaterProcessDefinition(processDefinition));
                }
                exchange.getOut().setBody(processDefinitionList);

            }
        })
                .setHeader(Exchange.CONTENT_TYPE, constant("application/json"))
                .marshal().json(JsonLibrary.Jackson);

        from("direct:getProcessInstances")
                .setHeader(Exchange.HTTP_RESPONSE_CODE, constant(HttpStatus.OK.value()))
                .setHeader(Exchange.HTTP_RESPONSE_TEXT, simple(HttpStatus.OK.getReasonPhrase()))
                .process(new Processor() {

                    public void process(Exchange exchange) throws Exception {
                        exchange.getOut().setHeader("Content-Type", "application/json");

                        List<ProcessInstance> processInstances = runtimeService.createProcessInstanceQuery().list();
                        List<ProcessInstanceDto> result = new ArrayList<>();

                        for (ProcessInstance processInstance : processInstances) {
                            ProcessInstanceDto dto = ProcessInstanceDto.fromProcessInstance(processInstance);
                            result.add(dto);
                        }


                        if (result == null) {
                            exchange.getOut().setHeader(Exchange.HTTP_RESPONSE_CODE, HttpStatus.NO_CONTENT.value());
                            exchange.getOut().setHeader(Exchange.HTTP_RESPONSE_TEXT, HttpStatus.NO_CONTENT.getReasonPhrase());


                        } else {
                            exchange.getOut().setBody(result);
                        }

                    }
                })
                .setHeader(Exchange.CONTENT_TYPE, constant("application/json"))
                .marshal().json(JsonLibrary.Jackson);

        from("direct:getProcessInstance").process(new Processor() {

            public void process(Exchange exchange) throws Exception {
                exchange.getOut().setHeader("Content-Type", "application/json");

                ProcessInstance processInstance = runtimeService.createProcessInstanceQuery().processInstanceId(exchange.getIn().getHeader("instanceId", String.class)).singleResult();

                ProcessInstanceDto dto = ProcessInstanceDto.fromProcessInstance(processInstance);

                AtomLink variables = new AtomLink("variables", exchange.getIn().getHeader("CamelHttpUri") + "variable/", "get");
                dto.addLink(variables);

                AtomLink activities = new AtomLink("activites", exchange.getIn().getHeader("CamelHttpUri") + "activity/", "get");
                dto.addLink(activities);

                if (dto == null) {
                    exchange.getOut().setHeader("status", "204");

                }

                exchange.getOut().setBody(dto);

            }
        }).setHeader(Exchange.CONTENT_TYPE, constant("application/json"))
                .marshal().json(JsonLibrary.Jackson);

        from("direct:getVariableInstances").process(new Processor() {

            public void process(Exchange exchange) throws Exception {
                exchange.getOut().setHeader("Content-Type", "application/json");

                List<VariableInstance> variableInstances = runtimeService.createVariableInstanceQuery().processInstanceIdIn(exchange.getIn().getHeader("instanceId", String.class)).list();
                List<VariableInstanceDto> result = new ArrayList<>();

                for (VariableInstance variableInstance : variableInstances) {
                    VariableInstanceDto dto = VariableInstanceDto.fromVariableInstance(variableInstance);
                    result.add(dto);
                }

                if (variableInstances == null) {
                    exchange.getOut().setHeader("status", "204");

                }

                exchange.getOut().setBody(result);

            }
        }).setHeader(Exchange.CONTENT_TYPE, constant("application/json"))
                .marshal().json(JsonLibrary.Jackson);

        from("direct:getActivities").process(new Processor() {

            public void process(Exchange exchange) throws Exception {
                exchange.getOut().setHeader("Content-Type", "application/json");

                HistoricActivityInstanceQuery q = historyService.createHistoricActivityInstanceQuery();

                if (exchange.getIn().getHeader("instanceId", String.class) != null) {
                    q.processInstanceId(exchange.getIn().getHeader("instanceId", String.class));
                }

                q.orderByHistoricActivityInstanceStartTime().asc();

                List<HistoricActivityInstance> activities = q.list();

                List<HistoricActivityInstanceDto> result = new ArrayList<>();

                for (HistoricActivityInstance activity : activities) {
                    HistoricActivityInstanceDto dto = HistoricActivityInstanceDto.fromHistoricActivityInstance(activity);
                    result.add(dto);
                }

                if (activities == null) {
                    exchange.getOut().setHeader("status", "204");

                }

                exchange.getOut().setBody(result);

            }
        }).setHeader(Exchange.CONTENT_TYPE, constant("application/json"))
                .marshal().json(JsonLibrary.Jackson);
        ;


        from("direct:ws0Call")
                .process(new Processor() {
                    @Override
                    public void process(Exchange exchange) throws Exception {
                        Map<String, Object> variables = (Map<String, Object>) exchange.getIn().getBody();

                        WS0In request = new WS0In();
                        List<String> images = (List<String>) variables.get("images");
                        List<PepsDataId> pepsDataIds = new ArrayList<>();

                        for (String image : images) {
                            PepsDataId pepsDataId = new PepsDataId();
                            pepsDataId.setId(image);
                            pepsDataIds.add(pepsDataId);
                        }

                        request.setPepsDataIds(pepsDataIds);

                        exchange.getOut().setBody(request);
                    }
                })
                .setHeader(Exchange.CONTENT_TYPE, constant("application/json"))
                .setHeader(Exchange.HTTP_METHOD, constant("POST"))
                .marshal().json(JsonLibrary.Jackson)
                .to("log:$body")
                .to("http://" + formaterHost + ":" + formaterWS0Port + formaterPrefix + formaterWs0 + "?authMethod=Basic&authUsername=" + userName + "&authPassword=" + password + "&mode=async")
                .unmarshal().json(JsonLibrary.Jackson, WS0Out.class)
                .to("log:$body");


        from("direct:ws1Call'")
                .setHeader(Exchange.CONTENT_TYPE, constant("application/json"))
                .setHeader(Exchange.HTTP_METHOD, constant("POST"))
                .to("log:$body")
                .to("http://" + formaterHost + ":" + "5023" + formaterPrefix + formaterWs1 + "?authMethod=Basic&authUsername=" + userName + "&authPassword=" + password + "&mode=async");

        from("direct:log")
                .to("log:$body");
    }
}
