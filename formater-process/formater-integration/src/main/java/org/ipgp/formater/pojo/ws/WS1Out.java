package org.ipgp.formater.pojo.ws;

import java.util.List;

/**
 * Created by olivier on 04/05/2017.
 */
public class WS1Out {

    private String jobId;

    private String processToken;

    public String getJobId() {
        return jobId;
    }

    public void setJobId(String jobId) {
        this.jobId = jobId;
    }

    public String getProcessToken() {
        return processToken;
    }

    public void setProcessToken(String processToken) {
        this.processToken = processToken;
    }
}
