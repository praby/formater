package org.ipgp.formater.pojo.ws;

import java.io.Serializable;

/**
 * Created by olivier on 04/05/2017.
 */
public class Result implements Serializable {

  private String resname;

    public String getResname() {
        return resname;
    }

    public void setResname(String resname) {
        this.resname = resname;
    }
}
