package org.ipgp.formater.pojo.ws;

import java.io.Serializable;

/**
 * Created by olivier on 04/05/2017.
 */
public class ResultWs7 implements Serializable {

  private String resname;

  private String resURI;

    public String getResname() {
        return resname;
    }

    public void setResname(String resname) {
        this.resname = resname;
    }

    public String getResURI() {
        return resURI;
    }

    public void setResURI(String resURI) {
        this.resURI = resURI;
    }
}
