package org.ipgp.formater.pojo;

import java.io.Serializable;
import java.util.List;

/**
 * Created by olivier on 04/05/2017.
 */
public class StartInterferogramme implements Serializable {

    private String email;

    private List<String> images;

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public List<String> getImages() {
        return images;
    }

    public void setImages(List<String> images) {
        this.images = images;
    }
}
