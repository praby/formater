package org.ipgp.formater.pojo.ws;

import java.util.List;

/**
 * Created by olivier on 04/05/2017.
 */
public class WS2In {

    private String type;

    private List<String> bbox;

    private String subSwath;

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public List<String> getBbox() {
        return bbox;
    }

    public void setBbox(List<String> bbox) {
        this.bbox = bbox;
    }

    public String getSubSwath() {
        return subSwath;
    }

    public void setSubSwath(String subSwath) {
        this.subSwath = subSwath;
    }
}
