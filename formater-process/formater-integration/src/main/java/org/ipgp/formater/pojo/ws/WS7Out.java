package org.ipgp.formater.pojo.ws;

import java.util.List;

/**
 * Created by olivier on 04/05/2017.
 */
public class WS7Out {

    private String processToken;

    private String subSwath;

    private List<ResultWs7> resNames;

    public String getProcessToken() {
        return processToken;
    }

    public void setProcessToken(String processToken) {
        this.processToken = processToken;
    }

    public String getSubSwath() {
        return subSwath;
    }

    public void setSubSwath(String subSwath) {
        this.subSwath = subSwath;
    }

    public List<ResultWs7> getResNames() {
        return resNames;
    }

    public void setResNames(List<ResultWs7> resNames) {
        this.resNames = resNames;
    }
}
