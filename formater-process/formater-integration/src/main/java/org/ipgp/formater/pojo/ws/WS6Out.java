package org.ipgp.formater.pojo.ws;

import java.util.List;

/**
 * Created by olivier on 04/05/2017.
 */
public class WS6Out {

    private String processToken;

    private String subSwath;

    private List<Result> resNames;

    public String getProcessToken() {
        return processToken;
    }

    public void setProcessToken(String processToken) {
        this.processToken = processToken;
    }

    public String getSubSwath() {
        return subSwath;
    }

    public void setSubSwath(String subSwath) {
        this.subSwath = subSwath;
    }

    public List<Result> getResNames() {
        return resNames;
    }

    public void setResNames(List<Result> resNames) {
        this.resNames = resNames;
    }
}
