package org.ipgp.formater.configuration;

import org.apache.camel.CamelContext;
import org.camunda.bpm.camel.common.CamelService;
import org.camunda.bpm.camel.spring.CamelServiceImpl;
import org.camunda.bpm.engine.ProcessEngine;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * Created by olivier on 04/05/2017.
 */
@Configuration
public class CamelInitContext {


    @Autowired
    private ProcessEngine processEngine;

    @Autowired
    private CamelContext camelContext;

    @Bean
    public CamelService camel(){
        CamelService camelService =  new CamelServiceImpl();
        ((CamelServiceImpl)camelService).setCamelContext(camelContext);
        ((CamelServiceImpl)camelService).setProcessEngine(processEngine);
        return camelService;
    }
}
