package org.ipgp.formater.pojo.ws;

import java.io.Serializable;
import java.util.List;

/**
 * Created by olivier on 04/05/2017.
 */
public class WS0In implements Serializable{

    private List<PepsDataId> pepsDataIds;

    public List<PepsDataId> getPepsDataIds() {
        return pepsDataIds;
    }

    public void setPepsDataIds(List<PepsDataId> pepsDataIds) {
        this.pepsDataIds = pepsDataIds;
    }
}
