package org.ipgp.formater.pojo.ws;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;

/**
 * Created by olivier on 05/05/2017.
 */
public class WS0Out implements Serializable{

    @JsonProperty("StatusInfo")
    private WS0OutStatus statusInfo;

    public WS0OutStatus getStatusInfo() {
        return statusInfo;
    }

    public void setStatusInfo(WS0OutStatus statusInfo) {
        this.statusInfo = statusInfo;
    }
}
