package org.ipgp.formater.pojo;

import org.camunda.bpm.engine.repository.ProcessDefinition;

import java.io.Serializable;

/**
 * Created by olivier on 04/05/2017.
 */
public class FormaterProcessDefinition implements Serializable {

    private String id;

    private String name;

    private String key;

    private String description;

    private boolean startFormKey;

    private boolean suspended;

    private String versionTag;

    public FormaterProcessDefinition(ProcessDefinition processDefinition){
        this.id = processDefinition.getId();
        this.name = processDefinition.getName();
        this.key = processDefinition.getKey();
        this.description = processDefinition.getDescription();
        this.startFormKey = processDefinition.hasStartFormKey();
        this.suspended = processDefinition.isSuspended();
        this.versionTag = processDefinition.getVersionTag();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public boolean isStartFormKey() {
        return startFormKey;
    }

    public void setStartFormKey(boolean startFormKey) {
        this.startFormKey = startFormKey;
    }

    public boolean isSuspended() {
        return suspended;
    }

    public void setSuspended(boolean suspended) {
        this.suspended = suspended;
    }

    public String getVersionTag() {
        return versionTag;
    }

    public void setVersionTag(String versionTag) {
        this.versionTag = versionTag;
    }

    public String getId() {

        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
