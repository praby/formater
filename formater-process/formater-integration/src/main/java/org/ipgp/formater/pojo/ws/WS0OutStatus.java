package org.ipgp.formater.pojo.ws;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;

/**
 * Created by olivier on 04/05/2017.
 */
public class WS0OutStatus implements Serializable {

    @JsonProperty("JobID")
    private String jobId;

    @JsonProperty("Progress")
    private String progress;

    private String processToken;

    @JsonProperty("Status")
    private String status;

    public String getProcessToken() {
        return processToken;
    }

    public void setProcessToken(String processToken) {
        this.processToken = processToken;
    }

    public String getJobId() {
        return jobId;
    }

    public void setJobId(String jobID) {
        this.jobId = jobID;
    }

    public String getProgress() {
        return progress;
    }

    public void setProgress(String progress) {
        this.progress = progress;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
