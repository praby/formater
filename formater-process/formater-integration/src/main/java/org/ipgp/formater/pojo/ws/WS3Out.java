package org.ipgp.formater.pojo.ws;

/**
 * Created by olivier on 04/05/2017.
 */
public class WS3Out {

    private String processToken;

    private String subSwath;

    public String getProcessToken() {
        return processToken;
    }

    public void setProcessToken(String processToken) {
        this.processToken = processToken;
    }

    public String getSubSwath() {
        return subSwath;
    }

    public void setSubSwath(String subSwath) {
        this.subSwath = subSwath;
    }
}
