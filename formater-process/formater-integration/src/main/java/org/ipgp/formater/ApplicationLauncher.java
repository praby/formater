package org.ipgp.formater;

import org.apache.camel.CamelContext;
import org.camunda.bpm.camel.common.CamelService;
import org.camunda.bpm.camel.spring.CamelServiceImpl;
import org.camunda.bpm.engine.ProcessEngine;
import org.camunda.bpm.engine.RepositoryService;
import org.restlet.Component;
import org.restlet.ext.spring.SpringServerServlet;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.ServletRegistrationBean;
import org.springframework.context.annotation.Bean;

import javax.annotation.PostConstruct;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by olivier on 29/03/2017.
 */
@SpringBootApplication
public class ApplicationLauncher {

    public static void main(final String[] args) {
        SpringApplication.run(ApplicationLauncher.class, args);
    }

    @Bean
    public ServletRegistrationBean servletRegistrationBean() {

        SpringServerServlet serverServlet = new SpringServerServlet();
        ServletRegistrationBean regBean = new ServletRegistrationBean(serverServlet, "/rest/*");


        Map<String,String> params = new HashMap<String, String>();

        params.put("org.restlet.component", "restletComponent");

        regBean.setInitParameters(params);

        return regBean;
    }

    @Bean
    public Component restletComponent() {
        return new Component();
    }

}
