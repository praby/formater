package org.ipgp.formater.process;

import org.apache.camel.test.spring.CamelSpringBootRunner;
import org.apache.camel.test.spring.DisableJmx;
import org.apache.camel.test.spring.MockEndpoints;
import org.camunda.bpm.engine.RepositoryService;
import org.camunda.bpm.engine.RuntimeService;
import org.camunda.bpm.engine.runtime.ProcessInstance;
import org.ipgp.formater.ApplicationLauncher;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.DirtiesContext;

import static org.springframework.test.annotation.DirtiesContext.ClassMode.AFTER_CLASS;

/**
 * Created by olivier on 04/05/2017.
 */
@RunWith(CamelSpringBootRunner.class)
@SpringBootTest(classes = ApplicationLauncher.class)
@DirtiesContext(classMode = AFTER_CLASS)
@MockEndpoints("log:*")
@DisableJmx(false)
public class MainProcessTest {

    @Autowired
    private RepositoryService repositoryService;

    @Autowired
    private RuntimeService runtimeService;

    @Before
    public void beforeEach(){
        repositoryService.createDeployment()
                .addClasspathResource("process-def/interferogramme_main.bpmn")
                .addClasspathResource("process-def/interferogramme_sous_fachee.bpmn")
                .deploy();

        System.out.println(repositoryService.createProcessDefinitionQuery().list());

    }

    @Test
    public void lancerProcess(){
        runtimeService.startProcessInstanceByKey("interferogramme-main");
    }

}
