package org.ipgp.formater.routes;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.camel.EndpointInject;
import org.apache.camel.Produce;
import org.apache.camel.ProducerTemplate;
import org.apache.camel.component.mock.MockEndpoint;
import org.apache.camel.test.spring.CamelSpringBootRunner;
import org.apache.camel.test.spring.DisableJmx;
import org.apache.camel.test.spring.MockEndpoints;
import org.camunda.bpm.engine.HistoryService;
import org.camunda.bpm.engine.RepositoryService;
import org.camunda.bpm.engine.RuntimeService;
import org.camunda.bpm.engine.history.HistoricActivityInstance;
import org.camunda.bpm.engine.history.HistoricVariableInstance;
import org.camunda.bpm.engine.repository.ProcessDefinition;
import org.camunda.bpm.engine.runtime.Execution;
import org.camunda.bpm.engine.runtime.ProcessInstance;
import org.ipgp.formater.ApplicationLauncher;
import org.ipgp.formater.pojo.StartInterferogramme;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.Commit;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.transaction.TransactionConfiguration;
import org.springframework.transaction.annotation.Transactional;

import java.util.Arrays;
import java.util.List;

import static org.springframework.test.annotation.DirtiesContext.ClassMode.AFTER_CLASS;

/**
 * Created by olivier on 04/05/2017.
 */
@RunWith(CamelSpringBootRunner.class)
@SpringBootTest(classes = ApplicationLauncher.class)
@DirtiesContext(classMode = AFTER_CLASS)
@DisableJmx(false)
@Commit
@Transactional
public class ProcessTest {

    @Autowired
    private RepositoryService repositoryService;

    @Produce(uri = "direct:getProcessList", context = "camelContext")
    protected ProducerTemplate processListEndpoint;

    @Produce(uri = "direct:mapStartProcess", context = "camelContext")
    protected ProducerTemplate processStartEndpoint;

    @Autowired
    private HistoryService historyService;

    @EndpointInject(uri = "mock:log", context = "camelContext")
    protected MockEndpoint mockEnd;

    @Before
    public void beforeEach(){
        repositoryService.createDeployment()
                .addClasspathResource("process-def/interferogramme_main.bpmn")
                .addClasspathResource("process-def/interferogramme_sous_fachee.bpmn")
                .addClasspathResource("process-def/call_ws.bpmn")
                .deploy();

        System.out.println(repositoryService.createProcessDefinitionQuery().list());

    }

    @Test
    public void testProcessDefinitions(){
        Object response = processListEndpoint.requestBody("toto");
        System.out.println(response);
    }

    @Test
    public void testStartProcessInstance() throws InterruptedException, JsonProcessingException {
        StartInterferogramme request = new StartInterferogramme();
        request.setEmail("test@gmail.com");
        request.setImages(Arrays.asList("abfdsfs","dfghdf"));

        ObjectMapper mapper = new ObjectMapper();

        String json = mapper.writeValueAsString(request);

        System.out.println(json);

        Object response = processStartEndpoint.requestBodyAndHeader(json, "id", "interferogramme-main");

        Thread.sleep(10000l);

        List<HistoricActivityInstance> historicActivityInstances = historyService.createHistoricActivityInstanceQuery().processInstanceId((String)response).activityType("callActivity").orderByHistoricActivityInstanceStartTime().asc().list();

        for(HistoricActivityInstance historicActivityInstance : historicActivityInstances){
            System.out.println(historicActivityInstance.getActivityId() + " - " + historicActivityInstance.getActivityType());
        }

        List<HistoricVariableInstance> variableInstances = historyService
                .createHistoricVariableInstanceQuery()
                .processInstanceId(historicActivityInstances.get(0).getCalledProcessInstanceId())
                .list();

        for (HistoricVariableInstance variable : variableInstances){
            System.out.println(variable);
            System.out.println(variable.getValue().getClass() + " - " + variable.getValue());

        }

    }
}
