package org.ipgp.formater.routes;

import org.apache.camel.CamelContext;
import org.apache.camel.EndpointInject;
import org.apache.camel.Produce;
import org.apache.camel.ProducerTemplate;
import org.apache.camel.component.mock.MockEndpoint;
import org.apache.camel.test.spring.*;
import org.ipgp.formater.ApplicationLauncher;
import org.junit.Test;
import org.junit.runner.RunWith;
import static org.springframework.test.annotation.DirtiesContext.ClassMode.*;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.BootstrapWith;
import org.springframework.test.context.ContextConfiguration;

/**
 * Created by olivier on 29/03/2017.
 */

@RunWith(CamelSpringBootRunner.class)
@SpringBootTest(classes = ApplicationLauncher.class)
@DirtiesContext(classMode = AFTER_CLASS)
@MockEndpoints("log:*")
@DisableJmx(false)
public class FormaterRoutesTest {

    @Autowired
    protected CamelContext camelContext;

    @Produce(uri = "direct:test", context = "camelContext")
    protected ProducerTemplate start;

    @EndpointInject(uri = "mock:log", context = "camelContext")
    protected MockEndpoint mockEnd;

    @Test
    public void testPositive() throws Exception {
        mockEnd.expectedBodiesReceived("hello");
        start.sendBody("hello");

        MockEndpoint.assertIsSatisfied(camelContext);
    }
}
