package org.ipgp.formater.routes;

import org.apache.camel.*;
import org.apache.camel.test.junit4.CamelTestSupport;
import org.apache.camel.test.spring.CamelSpringBootRunner;
import org.apache.camel.test.spring.MockEndpoints;
import org.ipgp.formater.ApplicationLauncher;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;


/**
 * Created by olivier on 29/03/2017.
 */

@RunWith(CamelSpringBootRunner.class)
@SpringBootTest(classes = ApplicationLauncher.class)
@MockEndpoints("http:*")
public class CamelContextTest{



    @Autowired
    private CamelContext camelContext;

    @Test
    public void testRoutesNumber(){
        System.out.println("Nombre de routes : "+camelContext.getRoutes().size());
    }

    @Test
    public void testRoute() throws InterruptedException {
        ProducerTemplate producerTemplate = camelContext.createProducerTemplate();
        ConsumerTemplate consumerTemplate = camelContext.createConsumerTemplate();


        String body = " {\n" +
                " \t\"pepsDataIds\": [{\n" +
                " \t\t\"id\": \"cfafa369-e89b-53d9-94bf-d7c68496970f\"\n" +
                " \t}, {\n" +
                " \t\t\"id\": \"f9f1b727-7a14-5b7c-96b0-456d53d3c1fe\"\n" +
                " \t}, {\n" +
                " \t\t\"id\": \"0ef5e877-7596-5166-b20f-94eea05933eb\"\n" +
                " \t}]\n" +
                " }";

        String s = producerTemplate.requestBody("direct:ws0", body, String.class);

        System.out.println(s);

    /*    Exchange exchange = consumerTemplate.receive("http://localhost:5022/v1.0/services/ws_dnldSar2Clstr?authMethod=Basic&authUsername=miguel&authPassword=python&mode=async");

        Thread.sleep(1000l);

        System.out.println(exchange);
        */
    }

    @Test
    public void testWS1() throws InterruptedException {
        ProducerTemplate producerTemplate = camelContext.createProducerTemplate();
        ConsumerTemplate consumerTemplate = camelContext.createConsumerTemplate();


        String body = "[{\"processToken\" : \"f5e6f9g8t5232n5d5d6s56\"}," +
                "{ \"type\": \"Feature\"," +
                " \"bbox\": [ 2.8784608840942383 , " +
                "42.694144332492674 , " +
                "2.8876025772094727 , " +
                "42.69814795493774] }]";

        String s = producerTemplate.requestBody("direct:ws1", body, String.class);

        System.out.println(s);

    }

    @Test
    public void testLancerProcess() throws InterruptedException {
        ProducerTemplate producerTemplate = camelContext.createProducerTemplate();
        ConsumerTemplate consumerTemplate = camelContext.createConsumerTemplate();


        String body = "[{\"processToken\" : \"f5e6f9g8t5232n5d5d6s56\"}," +
                "{ \"type\": \"Feature\"," +
                " \"bbox\": [ 2.8784608840942383 , " +
                "42.694144332492674 , " +
                "2.8876025772094727 , " +
                "42.69814795493774] }]";

        String s = producerTemplate.requestBody("http://", body, String.class);

        System.out.println(s);

    }

}
